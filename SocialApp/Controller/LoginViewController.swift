//
//  LoginViewController.swift
//  SocialApp
//
//  Created by Sem Vastuin on 29.03.17.
//  Copyright © 2017 Brander. All rights reserved.
//

import Foundation
import UIKit
import PKHUD

class LoginViewController: UIViewController {
    
    // MARK: - Private fields
    @IBOutlet fileprivate weak var emailTextField: UITextField!
    @IBOutlet fileprivate weak var passwordTextField: UITextField!
    @IBOutlet fileprivate weak var signInButton: UIButton!
    
    // MARK: - UIViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.signInFailed), name: .signInFailed, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.userSigned), name: .userSigned, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    // MARK: - Private methods
    @IBAction fileprivate func signInButtonPressed(_ sender: Any) {
        guard let email = emailTextField.text, let password = passwordTextField.text else { return }
        
        HUD.show(.progress)
        SessionDataManager.instance.signIn(withEmail: email, password: password)
    }

    // MARK: - Observed methods
    func userSigned() {
        HUD.hide()
        performSegue(withIdentifier: Utils.signInSegue, sender: nil)
    }
    
    func signInFailed() {
        HUD.flash(.error, delay:  0.3)
    }
}
