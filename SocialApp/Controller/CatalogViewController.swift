//
//  CatalogViewController.swift
//  SocialApp
//
//  Created by Sem Vastuin on 03.04.17.
//  Copyright © 2017 Brander. All rights reserved.
//

import UIKit
import PKHUD

class CatalogViewController: UITableViewController {
    
    // MARK: - UIViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.usersFetched), name: .usersFetched, object: nil)
        
        HUD.show(.progress)
        DataManager.instance.fetchUsers()
        
        tableView.register(CatalogViewCell.nibName, forCellReuseIdentifier: CatalogViewCell.identifier)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - Observer methods
    func usersFetched() {
        HUD.hide()
        tableView.reloadData()
    }
}

// MARK: - TableView data source
extension CatalogViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataManager.instance.userList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CatalogViewCell.identifier, for: indexPath) as! CatalogViewCell
        let currentUser = DataManager.instance.userList[indexPath.row]
        
        cell.updateCell(withUserData: currentUser)
        
        return cell
    }
}

// MARK: - TableView delegate
extension CatalogViewController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CatalogViewCell.height
    }
}
