//
//  InitialViewController.swift
//  SocialApp
//
//  Created by Sem Vastuin on 07.04.17.
//  Copyright © 2017 Brander. All rights reserved.
//

import UIKit
import PKHUD

class InitialViewController: UIViewController {
    
    @IBOutlet fileprivate weak var loginButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: Logout unwind
    @IBAction func unwindToInitial(segue: UIStoryboardSegue) {}
}
