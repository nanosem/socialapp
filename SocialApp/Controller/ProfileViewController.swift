//
//  ProfileViewController.swift
//  SocialApp
//
//  Created by Sem Vastuin on 31.03.17.
//  Copyright © 2017 Brander. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import Firebase

class ProfileViewController: UIViewController{
    
    // MARK: - Private fields
    @IBOutlet fileprivate weak var imageView: UIImageView!
    @IBOutlet fileprivate weak var firstNameLabel: UILabel!
    @IBOutlet fileprivate weak var lastNameLabel: UILabel!
    
    @IBOutlet fileprivate weak var postViewWrap: UIView!
    @IBOutlet fileprivate weak var tableView: UITableView!
    
    // MARK: - UIViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(PostViewCell.nibName, forCellReuseIdentifier: PostViewCell.identifier)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.userEdited), name: .userEdited, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.postInteracted), name: .postCreated, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.postInteracted), name: .postEdited, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.postInteracted), name: .postLiked, object: nil)
        
        // TODO: Need to refactor
        NotificationCenter.default.addObserver(self, selector: #selector(self.postInteracted), name: .postDisliked, object: nil)
        
        guard let user = SessionDataManager.instance.sessionUser, let imageUrl = user.imageUrl else { return }
        imageView.af_setImage(withURL: imageUrl)
        firstNameLabel.text = user.firstName
        lastNameLabel.text = user.lastName
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        setUpImageView()
    }
    
    // MARK: - Private functions
    fileprivate func setUpImageView() {
        imageView.layer.borderWidth = 0.3
        imageView.layer.borderColor = UIColor.darkGray.cgColor
        imageView.layer.cornerRadius = imageView.frame.width / 2
        imageView.clipsToBounds = true
    }
    
    // Observed methods
    func userEdited() {
        guard let user = SessionDataManager.instance.sessionUser else { return }
        
        imageView.af_setImage(withURL: user.imageUrl!)
        firstNameLabel.text = user.firstName
        lastNameLabel.text = user.lastName
    }
    
    func postInteracted() {
        tableView.reloadData()
    }
    
    @IBAction func unwingToProfile(segue: UIStoryboardSegue) {}
}

// MARK: - TableView DataSource
extension ProfileViewController: UITableViewDelegate{

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return SessionDataManager.instance.likedPosts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PostViewCell.identifier, for: indexPath) as! PostViewCell

        let currentPost = SessionDataManager.instance.likedPosts[indexPath.row]
        cell.updateCellData(withText: currentPost.text, forPostWithId: currentPost.id)
        
        return cell
    }
}

// MARK: - TableView Delegate
extension ProfileViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return PostViewCell.minHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
