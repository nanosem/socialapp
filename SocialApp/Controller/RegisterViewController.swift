//
//  RegisterViewController.swift
//  SocialApp
//
//  Created by Sem Vastuin on 29.03.17.
//  Copyright © 2017 Brander. All rights reserved.
//

import UIKit
import PKHUD

class RegisterViewController: UIViewController, UINavigationControllerDelegate {
    
    // MARK: - Private fields
    @IBOutlet fileprivate weak var photoImageView: UIImageView!
    @IBOutlet fileprivate weak var firstNameTextField: UITextField!
    @IBOutlet fileprivate weak var lastNameTextField: UITextField!
    @IBOutlet fileprivate weak var emailTextField: UITextField!
    @IBOutlet fileprivate weak var passwordTextField: UITextField!
    
    // MARK: - UIViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.userSigned), name: .userSigned, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.signUpFailed), name: .signUpFailed, object: nil)
        
        photoImageView.image = UIImage(named: "profile.png")
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setUpImageView()
    }
    
    // MARK: - Private actions
    @IBAction fileprivate func registerButtonClicked(_ sender: Any) {
        guard let firstName = firstNameTextField.text,
            let lastName = lastNameTextField.text,
            let email = emailTextField.text,
            let password = passwordTextField.text,
            let image = photoImageView.image else { return }
        
        HUD.show(.progress)
        NetworkDataManager.instance.getUrl(fromImage: image, withEmail: email) { imageUrl in
            let id = Utils.randomString
            let user = User(id: id, imageUrl: imageUrl, firstName: firstName, lastName: lastName, email: email, password: password, signedOnList: nil, postsID: nil, likedNews: nil)
            SessionDataManager.instance.signUp(user: user)
        }
    }
    
    fileprivate func setUpImageView() {
        photoImageView.layer.borderWidth = 0.3
        photoImageView.layer.borderColor = UIColor.darkGray.cgColor
        photoImageView.layer.cornerRadius = photoImageView.frame.width / 2
        photoImageView.clipsToBounds = true
    }
    
    // MARK: - Observer methods
    func userSigned() {
        HUD.hide()
        performSegue(withIdentifier: Utils.registerSegue, sender: nil)
    }
    
    func signUpFailed() {
        HUD.flash(.error, delay: 0.3)
    }
}

// MARK: - UIImagePickerControllerDelegate
extension RegisterViewController: UIImagePickerControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        self.photoImageView.image = image
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    @IBAction fileprivate func checkImageButtonClicked(_ sender: Any) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { [weak self] action in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                imagePicker.sourceType = .camera
                self?.present(imagePicker, animated: true, completion: nil)
            }
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Photo library", style: .default, handler: { [weak self] action in
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                imagePicker.sourceType = .photoLibrary
                self?.present(imagePicker, animated:  true, completion: nil)
            }
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(actionSheet, animated: true, completion: nil)
    }
}
