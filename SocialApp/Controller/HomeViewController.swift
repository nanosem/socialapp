//
//  HomeViewController.swift
//  SocialApp
//
//  Created by Sem Vastuin on 31.03.17.
//  Copyright © 2017 Brander. All rights reserved.
//

import UIKit

class HomeViewController: UITableViewController {
    
    // MARK: - UIViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.register(PostViewCell.nibName, forCellReuseIdentifier: PostViewCell.identifier)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.postInteracted), name: .postCreated, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.postInteracted), name: .postEdited, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - Segue functions
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Utils.homeToEditPost {
            let destination = segue.destination as! EditPostViewController
            destination.editedPost = sender as? Post
        }
    }
    
    // MARK: - Observed methods
    func postInteracted() {
        tableView.reloadData()
    }
}

// MARK: - TableView DataSource
extension HomeViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return SessionDataManager.instance.postsList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PostViewCell.identifier, for: indexPath) as! PostViewCell
        
        let currentPost = SessionDataManager.instance.postsList[indexPath.row]
        cell.updateCellData(withText: currentPost.text, forPostWithId: currentPost.id)
        
        return cell
    }
}

// MARK: - TableView Delegate
extension HomeViewController {

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        guard let sessionUser = SessionDataManager.instance.sessionUser, let postsID = sessionUser.postsID else { return false }
        let post = SessionDataManager.instance.postsList[indexPath.row]
        
        if postsID.contains(post.id) {
            return true
        } else {
            return false
        }
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let post = SessionDataManager.instance.postsList[indexPath.row]
            DataManager.instance.delete(post: post)
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return PostViewCell.standartHeight
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        guard let sessionUser = SessionDataManager.instance.sessionUser, let postsID = sessionUser.postsID else { return }
        let post = SessionDataManager.instance.postsList[indexPath.row]
        
        if postsID.contains(post.id) {
            let currentPost = SessionDataManager.instance.postsList[indexPath.row]
            
            performSegue(withIdentifier: Utils.homeToEditPost, sender: currentPost)
        }
    }
}
