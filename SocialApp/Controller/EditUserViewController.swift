//
//  EditUserViewController.swift
//  SocialApp
//
//  Created by Sem Vastuin on 03.04.17.
//  Copyright © 2017 Brander. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import PKHUD

class EditUserViewController: UIViewController, UINavigationControllerDelegate {
    
    // MARK: - Private fields
    @IBOutlet fileprivate weak var imageView: UIImageView!
    @IBOutlet fileprivate weak var firstNameTextField: UITextField!
    @IBOutlet fileprivate weak var lastNameTextField: UITextField!
    
    // MARK: - UIViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let user = SessionDataManager.instance.sessionUser, let imageUrl = user.imageUrl else { return }
        
        setUpImageView()
        
        imageView.image = UIImage(named: "profile.png")
        imageView.af_setImage(withURL: imageUrl)
        firstNameTextField.text = user.firstName
        lastNameTextField.text = user.lastName
    }
    
    // MARK: - Private actions
    fileprivate func setUpImageView() {
        imageView.layer.borderWidth = 0.25
        imageView.layer.borderColor = UIColor.darkGray.cgColor
        imageView.layer.cornerRadius = imageView.frame.width / 2
        imageView.clipsToBounds = true
    }
    
    @IBAction fileprivate func doneButtonPressed(_ sender: Any) {
        guard let user = SessionDataManager.instance.sessionUser else { return }
        guard let image = imageView.image, let email = user.email else { return }
        
        HUD.show(.progress)
        NetworkDataManager.instance.getUrl(fromImage: image, withEmail: email) { [weak self] imageUrl in
            let firstName = self?.firstNameTextField.text
            let lastName = self?.lastNameTextField.text
            
            let editedUser = User(id: user.id, imageUrl: imageUrl, firstName: firstName, lastName: lastName, email: email, password: user.password, signedOnList: nil, postsID: nil, likedNews: nil)
            
            DataManager.instance.edit(user: editedUser)
            
            HUD.flash(.success, delay: 0.3)
            _ = self?.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func logoutButtonPressed(_ sender: Any) {
        SessionDataManager.instance.logout()
    }
}

// MARK: - UIImagePickerControllerDelegate
extension EditUserViewController: UIImagePickerControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        imageView.image = image
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func checkImageButttonPressed(_ sender: Any) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { [weak self] action in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                imagePicker.sourceType = .camera
                self?.present(imagePicker, animated: true, completion: nil)
            }
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Photo library", style: .default, handler: { [weak self] action in
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                imagePicker.sourceType = .photoLibrary
                self?.present(imagePicker, animated:  true, completion: nil)
            }
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(actionSheet, animated: true, completion: nil)
    }
}
