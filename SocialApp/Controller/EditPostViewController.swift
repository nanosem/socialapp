//
//  EditPostViewController.swift
//  SocialApp
//
//  Created by Sem Vastuin on 04.04.17.
//  Copyright © 2017 Brander. All rights reserved.
//

import UIKit

class EditPostViewController: UIViewController {

    // MARK: - Private Fields
    @IBOutlet fileprivate weak var textView: UITextView!
    
    var editedPost: Post?
    
    // MARK: - UIViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if editedPost != nil {
            textView.text = editedPost?.text
        }
    }
    
    // MARK: - Private actions
    @IBAction fileprivate func savePost(_ sender: Any) {
        if editedPost == nil {
            let id = Utils.randomString
            guard let text = self.textView.text else { return }
            
            let post = Post(id: id, text: text)
            DataManager.instance.add(post: post)
        } else {
            editedPost?.text = textView.text
            DataManager.instance.edit(post: editedPost!)
        }
        _ = navigationController?.popViewController(animated: true)
    }

    @IBAction fileprivate func cancelButtonPressed(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
}
