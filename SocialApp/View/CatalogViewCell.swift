//
//  CatalogViewCell.swift
//  SocialApp
//
//  Created by Sem Vastuin on 03.04.17.
//  Copyright © 2017 Brander. All rights reserved.
//

import UIKit
import AlamofireImage

class CatalogViewCell: UITableViewCell {
    
    // MARK: - Private fields
    @IBOutlet fileprivate weak var followButton: UIButton!
    @IBOutlet fileprivate weak var photoImageView: UIImageView!
    @IBOutlet fileprivate weak var nameTextField: UILabel!
    
    fileprivate var user: User!
    
    // MARK: - Static fields
    static let nibName = UINib(nibName: "\(CatalogViewCell.self)", bundle: nil)
    static let identifier = "\(CatalogViewCell.self)"
    static let height = CGFloat(64)
    
    // MARK: - UITableViewCell
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: - Private actions and functions
    @IBAction private func followButtonPressed(_ sender: Any) {
        
        guard user != nil else { return }
        
        let signedOnList = SessionDataManager.instance.signedOnList
        if signedOnList.contains(user) {
            DataManager.instance.unFollow(user: user)
            followButton.backgroundColor = UIColor(red:0.01, green:0.67, blue:0.00, alpha:1.0) //Green
        } else {
            DataManager.instance.follow(user: user)
            followButton.backgroundColor = UIColor(red:0.89, green:0.36, blue:0.47, alpha:1.0) //Red
        }
    }
    
    fileprivate func setUpImageView() {
        photoImageView.layer.borderWidth = 0.3
        photoImageView.layer.borderColor = UIColor.darkGray.cgColor
        photoImageView.layer.cornerRadius = photoImageView.frame.width / 2
        photoImageView.clipsToBounds = true
    }
    
    // MARK: Methods
    func updateCell(withUserData user: User) {
        // TODO: Need to refactor
        self.user = user
        guard let imageUrl = user.imageUrl, let firstName = user.firstName, let lastName = user.lastName else { return }
        
        nameTextField.text = "\(firstName) \(lastName)"
        photoImageView.af_setImage(withURL: imageUrl)
        
        guard let sessionUser = SessionDataManager.instance.sessionUser,
            let email = user.email,
            let signedOnList = sessionUser.signedOnList else { return }
        
        let key = Utils.convertToKey(string: email)
        if signedOnList.contains(key) {
            followButton.setTitle("Unfollow", for: .normal)
            followButton.backgroundColor = UIColor(red:0.89, green:0.36, blue:0.47, alpha:1.0) //Red
        } else {
            followButton.setTitle("Follow", for: .normal)
            followButton.backgroundColor = UIColor(red:0.01, green:0.67, blue:0.00, alpha:1.0) //Green
        }
    }
}
