//
//  PostViewCell.swift
//  SocialApp
//
//  Created by Sem Vastuin on 04.04.17.
//  Copyright © 2017 Brander. All rights reserved.
//

import UIKit

class PostViewCell: UITableViewCell {

    // MARK: - Private fields
    @IBOutlet fileprivate weak var postContentLabel: UILabel!
    @IBOutlet fileprivate weak var likeButton: UIButton!
    fileprivate var postId: String!
    
    // MARK: - Static fields
    static let nibName = UINib(nibName: "\(PostViewCell.self)", bundle: nil)
    static let identifier = "\(PostViewCell.self)"
    static let standartHeight = CGFloat(75)
    static let minHeight = CGFloat(65)
    
    // MARK: - UITableViewcell
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: - Methods
    func updateCellData(withText text: String, forPostWithId postId: String) {
        self.postContentLabel.text = text
        self.postId = postId
    }
    
    @IBAction fileprivate func likeButtonPressed(_ sender: Any) {
        let post = SessionDataManager.instance.postsList.filter{ $0.id == postId }.first
        guard post != nil else { return }
        
        if SessionDataManager.instance.likedPosts.contains(post!) {
            DataManager.instance.dislike(post: post!)
        } else {
            DataManager.instance.like(post: post!)
        }
    }
}
