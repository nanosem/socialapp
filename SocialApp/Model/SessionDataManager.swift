//
//  SessionDataManager.swift
//  SocialApp
//
//  Created by Sem Vastuin on 04.04.17.
//  Copyright © 2017 Brander. All rights reserved.
//

import UIKit

class SessionDataManager {
    
    // MARK: - Singleton
    static let instance = SessionDataManager()
    private init() {}
    
    // MARK: - Session Data
    fileprivate(set) var sessionUser: User?
    fileprivate(set) var signedOnList = [User]()
    fileprivate(set) var postsList = [Post]()
    fileprivate(set) var likedPosts = [Post]()
}


// MARK: - Auth methods
extension SessionDataManager {
    
    func signUp(user: User) {
        NetworkDataManager.instance.signUp(user: user) { [weak self] user in
            if user == nil {
                NotificationCenter.default.post(name: .signUpFailed, object: nil)
                return
            }
            self?.sessionUser = user
            NotificationCenter.default.post(name: .userSigned, object: nil)
        }
    }
    
    func signIn(withEmail email: String, password: String) {
        NetworkDataManager.instance.getUserData(withEmail: email, password: password) { [weak self] user in
            
            guard user != nil else {
                NotificationCenter.default.post(name: .signInFailed, object: nil)
                return
            }
            self?.sessionUser = user
            self?.getSignedOnList(forUser: user!)
            self?.getPostList(forUser: user!)
            self?.getLikedPostList(forUser: user!)
            
            NotificationCenter.default.post(name: .userSigned, object: nil)
        }
    }
    
    func getSignedOnList(forUser user: User) {
        NetworkDataManager.instance.getSignedOnList(forUser: user, { [weak self] usersList in
            if usersList == nil { return }
            
            for user in usersList! {
                guard let email = user.email else { return }
                let key = Utils.convertToKey(string: email)
                
                self?.sessionUser?.signedOnList?.append(key)
                if self?.sessionUser?.signedOnList == nil {
                    self?.sessionUser?.signedOnList = [key]
                }
            }
            self?.signedOnList = usersList!
        })
    }
    
    func getPostList(forUser user: User) {
        NetworkDataManager.instance.getPostList(forUser: user, { [weak self] userPostsList, followedPostsList in
            if userPostsList != nil {
                for post in userPostsList! {
                    self?.sessionUser?.postsID?.append(post.id)
                    if self?.sessionUser?.postsID == nil {
                        self?.sessionUser?.postsID = [post.id]
                    }
                }
                self?.postsList = userPostsList!
            }
            
            if followedPostsList != nil {
                self?.postsList.append(contentsOf: followedPostsList!)
            }
        })
    }
    
    func getLikedPostList(forUser user: User) {
        NetworkDataManager.instance.getLikedPostList(forUser: user, { [weak self] likedPosts in
            if likedPosts != nil {
                self?.sessionUser?.likedNews = nil
                for post in likedPosts! {
                    self?.sessionUser?.likedNews?.append(post.id)
                    if self?.sessionUser?.likedNews == nil {
                        self?.sessionUser?.likedNews = [post.id]
                    }
                }
                self?.likedPosts = likedPosts!
            }
        })
    }
    
    func logout() {
        sessionUser = nil
        signedOnList = []
        postsList = []
        likedPosts = []
        
        NotificationCenter.default.post(name: .userLoggedOut, object: nil)
    }
}

// MARK: - SessionUser Interaction methods
extension SessionDataManager {
    
    func edit(user: User) {
        self.sessionUser = user
    }
}

// MARK: - PostIntercation methods
extension SessionDataManager: PostInteraction {
    func add(post: Post) {
        sessionUser?.postsID?.append(post.id)
        if sessionUser?.postsID == nil {
            sessionUser?.postsID = [post.id]
        }
        
        postsList.append(post)
    }
    
    func edit(post: Post) {
        guard let index = postsList.index(of: post) else { return }
        postsList[index] = post
    }
    
    func delete(post: Post) {
        guard let index = sessionUser?.postsID?.index(of: post.id), let postIndex = postsList.index(of: post) else { return }
        sessionUser?.postsID?.remove(at: Int(index))
        
        postsList.remove(at: postIndex)
    }
    
    func like(post: Post) {
        sessionUser?.likedNews?.append(post.id)
        if sessionUser?.likedNews == nil {
            sessionUser?.likedNews = [post.id]
        }
        
        likedPosts.append(post)
    }
    
    func dislike(post: Post) {
        let index = sessionUser?.likedNews?.index(of: post.id)
        sessionUser?.likedNews?.remove(at: index!)
        
        let postIndex = likedPosts.index(of: post)
        likedPosts.remove(at: postIndex!)
    }
}

// MARK: - Follow / Unfollow methods
extension SessionDataManager {
    func follow(user: User) {
        
        guard let email = user.email else { return }
        let key = Utils.convertToKey(string: email)
        
        sessionUser?.signedOnList?.append(key)
        if sessionUser?.signedOnList == nil {
            sessionUser?.signedOnList = [key]
        }
        
        signedOnList.append(user)
    }
    
    func unFollow(user: User) {
        guard let email = user.email else { return }
        let key = Utils.convertToKey(string: email)
        
        guard let index = sessionUser?.signedOnList?.index(of: key), let userIndex = signedOnList.index(of: user) else { return }
        
        sessionUser?.signedOnList?.remove(at: index)
        
        signedOnList.remove(at: userIndex)
    }
}
