//
//  UserInteraction.swift
//  SocialApp
//
//  Created by Sem Vastuin on 30.03.17.
//  Copyright © 2017 Brander. All rights reserved.
//

import Foundation

protocol PostInteraction {
    func add(post: Post)
    func edit(post: Post)
    func delete(post: Post)
}
