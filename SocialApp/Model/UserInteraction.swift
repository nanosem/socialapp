//
//  UserInteraction.swift
//  SocialApp
//
//  Created by Sem Vastuin on 03.04.17.
//  Copyright © 2017 Brander. All rights reserved.
//

import Foundation

protocol UserInteraction {
    func edit(user: User)
    func delete(user: User)
}
