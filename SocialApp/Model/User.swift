//
//  User.swift
//  SocialApp
//
//  Created by Sem Vastuin on 30.03.17.
//  Copyright © 2017 Brander. All rights reserved.
//

import Foundation
import UIKit

struct User {
    
    var id: String?
    var imageUrl: URL?
    var firstName: String?
    var lastName: String?
    var email: String?
    var password: String?
    
    var signedOnList: [String]?
    var postsID: [String]?
    var likedNews: [String]?
    
    init (id: String?, imageUrl: URL?, firstName: String?, lastName: String?, email: String?, password: String?, signedOnList: [String]?, postsID: [String]?, likedNews: [String]?) {
        
        self.id = id
        self.imageUrl = imageUrl
        self.firstName = firstName
        self.lastName = lastName
        self.email = email
        self.password = password
        
        self.signedOnList = signedOnList
        self.postsID = postsID
        self.likedNews = likedNews
    }
}

extension User : Equatable {
    static func ==(lhs: User, rhs: User) -> Bool {
        if  lhs.id == rhs.id  {
            return true
        }
        return false
    }
}
