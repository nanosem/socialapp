//
//  Datamanager.swift
//  SocialApp
//
//  Created by Sem Vastuin on 30.03.17.
//  Copyright © 2017 Brander. All rights reserved.
//

import UIKit


class DataManager {
    
    // MARK: - Singleton
    static let instance = DataManager()
    private init() {}
    
    // MARK: - Data content
    fileprivate(set) var userList = [User]()
    
    // MARK: - DataManager methods
    func fetchUsers() {
        NetworkDataManager.instance.fetchUsers() { [weak self] users in
            self?.userList = users
            NotificationCenter.default.post(name: .usersFetched, object: nil)
        }
    }
}

// MARK: - PostInteraction Methods
extension DataManager: PostInteraction {
    func add(post: Post) {
        SessionDataManager.instance.add(post: post)
        NetworkDataManager.instance.add(post: post)
        NotificationCenter.default.post(name: .postCreated, object: nil)
    }
    
    func edit(post: Post) {
        guard let sessionUser = SessionDataManager.instance.sessionUser, let postsList = sessionUser.postsID else { return }
        if !postsList.contains(post.id) { return }
        
        SessionDataManager.instance.edit(post: post)
        NetworkDataManager.instance.edit(post: post)
        NotificationCenter.default.post(name: .postEdited, object: nil)
    }
    
    func delete(post: Post) {
        SessionDataManager.instance.delete(post: post)
        NetworkDataManager.instance.delete(post: post)
        NotificationCenter.default.post(name: .postDeleted, object: nil)
    }
    
    func like(post: Post) {
        SessionDataManager.instance.like(post: post)
        NetworkDataManager.instance.like(post: post)
        NotificationCenter.default.post(name: .postLiked, object: nil)
    }
    
    func dislike(post: Post) {
        SessionDataManager.instance.dislike(post: post)
        NetworkDataManager.instance.dislike(post: post)
        NotificationCenter.default.post(name: .postDisliked, object: nil)
    }
}

extension DataManager {
    func follow(user: User) {
        SessionDataManager.instance.follow(user: user)
        NetworkDataManager.instance.follow(user: user)
        NotificationCenter.default.post(name: .userFollowed, object: nil)
    }
    
    func unFollow(user: User) {
        SessionDataManager.instance.unFollow(user: user)
        NetworkDataManager.instance.unFollow(user: user)
        NotificationCenter.default.post(name: .userUnFollowed, object: nil)
    }
    
    func edit(user: User) {
        SessionDataManager.instance.edit(user: user)
        NetworkDataManager.instance.edit(user: user)
        NotificationCenter.default.post(name: .userEdited, object: nil)
    }
}
