//
//  Post.swift
//  SocialApp
//
//  Created by Sem Vastuin on 31.03.17.
//  Copyright © 2017 Brander. All rights reserved.
//

import Foundation

struct Post {
    var id: String
    var text: String
    var time: Data
    
    init(id: String, text: String) {
        self.id = id
        self.text = text
        self.time = Data()
    }
}


extension Post : Equatable {
    static func ==(lhs: Post, rhs: Post) -> Bool {
        if  lhs.id == rhs.id  {
            return true
        }
        return false
    }
}
