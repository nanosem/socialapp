//
//  Notification+Extension.swift
//  SocialApp
//
//  Created by Sem Vastuin on 30.03.17.
//  Copyright © 2017 Brander. All rights reserved.
//

import Foundation

extension Notification.Name{
    static let userSigned = Notification.Name("userSigned")
    static let signInFailed = Notification.Name("signInFailed")
    static let signUpFailed = Notification.Name("signUpFailed")
    static let userLoggedOut = Notification.Name("userLoggedOut")
    
    static let userEdited = Notification.Name("userEdited")
    static let usersFetched = Notification.Name("usersFetched")
    
    static let postCreated = Notification.Name("postCreated")
    static let postEdited = Notification.Name("postEdited")
    static let postDeleted = Notification.Name("postDeleted")
    
    static let userFollowed = Notification.Name("userFollowed")
    static let userUnFollowed = Notification.Name("userUnFollowed")
    
    static let postLiked = Notification.Name("postLiked")
    static let postDisliked = Notification.Name("postDisliked")
}
