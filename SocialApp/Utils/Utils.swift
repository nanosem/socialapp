//
//  Utils.swift
//  SocialApp
//
//  Created by Sem Vastuin on 29.03.17.
//  Copyright © 2017 Brander. All rights reserved.
//

import Foundation

struct Utils {
    
    // MARK: - SegueIdentifiers
    static let signInSegue = "signInSegue"
    static let registerSegue = "registerSegue"
    static let profileToEditPost = "profileToEditPost"
    static let homeToEditPost = "homeToEditPost"
    
    // MARK: - Generate random string
    static var randomString: String {
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)
        
        var randomString = ""
        
        for _ in 0 ..< 20 {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        return randomString
    }
    
    static func convertToKey(string: String) -> String {
        let key: [Character] = string.characters.filter{ $0 != "."}
        return String(key)
    }
}
